import { ParallaxEffect, LogoAnimation } from "./js/effects.js";
let slides = [];
let buttonPrev;
let buttonNext;
let dots;
let labels;
let animalsSlides;
let index = 0;
let body;
let axisX;
let axisY
let bg;
let paw;
let animalName;
const animalsNameTab = ["Bear", "Lama", "Tiger", "Parrot", "Panther"];

document.addEventListener('DOMContentLoaded', function(){
    // ParallaxEffect(index,axisX,axisY);
    // LogoAnimation();

    slides = document.querySelectorAll('.slide');
    animalsSlides = document.querySelectorAll('.img--an');
    buttonNext = document.querySelector('#next');
    buttonPrev = document.querySelector('#prev');
    dots = document.querySelectorAll('input');
    labels = document.querySelectorAll('label');
    body = document.querySelector('body');
    bg = document.querySelectorAll('.img--bg');
    paw = document.querySelector('.logo__icon');
    animalName = document.querySelector('.animalName');

    body.scrollLeft = 0;

    body.addEventListener('mousemove', function(e){
        axisX = 100 + e.pageX/100 + '%';
        axisY = 100 + e.pageY/100 + '%';
        ParallaxEffect(index, axisX, axisY, slides, bg);
        
    });//efekt
    buttonNext.addEventListener('click', function(){
        
        if (index < slides.length-1){
            index = index + 1;
        }
        else {
            index = 0;
        }
        LogoAnimation(paw);
        ShowSlide(index);
        HideSlide(index, slides);
        ChangeAnimal(index);
        ActiveDot(index, labels);
        ChangeAnimalsName(index);
    });
    buttonPrev.addEventListener('click', function(){
        
        if(index == 0){
            index = slides.length - 1;
        }
        else {
            index = index - 1;
        }
        LogoAnimation(paw);
        ShowSlide(index);
        HideSlide(index, slides);
        ChangeAnimal(index);
        ActiveDot(index, labels);
        ChangeAnimalsName(index);
    });
    for(let i = 0; i <= dots.length-1; i++){
        dots[i].addEventListener('click', function(){
            index = i;
            ShowSlide(index);
            HideSlide(index, slides);
            ChangeAnimal(index);
            ActiveDot(index, labels);
            ChangeAnimalsName(index);
            LogoAnimation(paw);
        });
    };
    ChangeAnimal(index);
    ActiveDot(index, labels);
    ShowSlide(index);
    ParallaxEffect(index, axisX, axisY, slides, bg);
});
function ShowSlide(index){
    slides[index].style.setProperty("display","flex");
};
function HideSlide(index, slides){
    for(let i = 0; i < slides.length; i++){
        if(i != index){
            slides[i].style.setProperty("display", "none");
        }
    }
};
function ChangeAnimal(index){
    for(let i = 0; i < animalsSlides.length; i++){
        if(i != index){
            animalsSlides[i].style.setProperty("display", "none");
        }
        else
        animalsSlides[index].style.setProperty("display","flex");
    }
}
function ChangeAnimalsName(index){
    animalName.innerText = animalsNameTab[index];
};
function ActiveDot(index, labels){
    for(let i = 0; i <= labels.length-1; i++){
        if(i == index){
            labels[i].style.setProperty("background-color", "whitesmoke");
        }
        else{
            labels[i].style.setProperty("background-color","rgba(53, 53, 53, 55%)");
        }
    }
};